1. deploy client app
1. check service without tls
```
kubectl -n canary-lukaszb  exec  $(kubectl get -n canary-lukaszb pod -l run=client -o jsonpath='{.items[0].metadata.name}')  -c istio-proxy -- curl -k -v nodejs:8080/sharks   
```
2. create policy
3. create destinationrule
4. check without tls
```
kubectl -n canary-lukaszb  exec  $(kubectl get -n canary-lukaszb pod -l run=client -o jsonpath='{.items[0].metadata.name}')  -c istio-proxy -- curl -k -v nodejs:8080/sharks   
```
5. check with certs
```
kubectl -n canary-lukaszb  exec  $(kubectl get -n canary-lukaszb pod -l run=client -o jsonpath='{.items[0].metadata.name}')  -c istio-proxy -- curl -k -v https://nodejs:8080/sharks --key /etc/certs/key.pem --cert /etc/certs/cert-chain.pem --cacert /etc/certs/root-cert.pem    
```

6. Cleanup!!!
```
kubectl delete -f .
```

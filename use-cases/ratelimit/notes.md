1. kubectl -n bookinfo exec -ti tools-6646bf8685-t88g6 -c istio-proxy -- bash
   From inside of container:
     while true; do curl -o /dev/null -s -w '%{http_code}\n' productpage:8080/productpage; done

2. cat ratelimit/\*
3. kubectl apply -f ratelimit/
4. From container http code 429 is visible
5. Show on kiali how traffic looks like
6. Show web app, log in to use session.
7. From inside of container:
     for i in $(seq 1 100); do do curl -o /dev/null -s -w '%{http_code}\n' productpage:8080/productpage; done
   and
     for i in $(seq 1 100); do do curl -o /dev/null -s -w '%{http_code}\n' -H 'Cookie: session=2390rufsdnofijs90f34fekj0394fi' productpage:8080/productpage; done
   to show difference in result for user which is logged in and not logged in
8. Cleanup
     kubectl delete -f ratelimit/

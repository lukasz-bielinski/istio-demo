1. delete mtls configs!
2. show that access is possible
```
 curl -v -k   a838ec9e9cd4d11e9bf010a5f21703b8-1160810151.us-east-1.elb.amazonaws.com/sharks
```
3. create ppolicy, enable jwt Authorization, use static token, show policy as it will take 20s to propagate
4. token
```
TOKEN=$(curl -k https://raw.githubusercontent.com/istio/istio/release-1.0/security/tools/jwt/samples/demo.jwt -s); echo $TOKEN
```
4. access is no longer possible
5. acess with token
```
curl -v -k --header "Authorization: Bearer $TOKEN"   a838ec9e9cd4d11e9bf010a5f21703b8-1160810151.us-east-1.elb.amazonaws.com/sharks
```

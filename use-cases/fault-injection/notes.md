1. kubectl -n bookinfo exec -ti tools-6646bf8685-t88g6 -c istio-proxy -- bash
   From inside of container:
     while true; do curl -o /dev/null -s -w '%{http_code} %{total_time}\n' productpage:8080/productpage; done
   Http codes 200 are visible
2. cat fault-injection/virtual-service-ratings-test-abort.yaml
3. kubectl apply -f fault-injection/virtual-service-ratings-test-abort.yaml
4. Codes 200 still visible
5. Show how traffic looks like on Kiali. Show on kiali where are traffic errors and http codes 500.
6. Show actual app web interface.
8. Cleanup
     kubectl delete -f fault-injection/virtual-service-ratings-test-abort.yaml
9. cat fault-injection/virtual-service-ratings-test-delay.yaml
10. kubectl apply -f fault-injection/virtual-service-ratings-test-delay.yaml
11. Request time is increased.
12. Show on kiali where is actual delay
13. Show Jeager and trace
14. Cleanup
      kubectl delete -f fault-injection/virtual-service-ratings-test-delay.yaml
15. Show grafana dashboards.
